#include <iostream>
using std::cout;
using std::cin;
using std::endl;

// function prototypes -- each function performs similar actions
void function_A( int );
void function_B( int );
void function_C( int );

int main()
{

    // void (*func)( int );
    // func=function_A;
    // func(99);
    // func=function_B;
    // func(99);
    // func=function_C;
    // func(99);

    int choice;

    void (*f[ 3 ])( int ) = { function_A, function_B, function_C };
    cout << "Enter a number between 0 ~ 2, otherwise the programe was exied ";
    cin >> choice;

    // process user's choice
    while ( ( choice >= 0 ) && ( choice < 3 ) ) 
    {
        // invoke the function at location choice in
        // the array f and pass choice as an argument
        (*f[ choice ])( choice ); 

        cout << "Enter a number between 0 ~ 2, otherwise the programe was exied ";
        cin >> choice;
    } 

    cout << "over the number." << endl;
    return 0; // indicates successful termination
} // end main

void function_A( int a )
{
   cout << "You entered " << a << " so function_A was called\n\n";
} 

void function_B( int b )
{
   cout << "You entered " << b << " so function_B was called\n\n";
} 

void function_C( int c )
{
   cout << "You entered " << c << " so function_C was called\n\n";
} 
